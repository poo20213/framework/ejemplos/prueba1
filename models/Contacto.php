<?php
namespace app\models;

use yii\base\Model;
use Yii;

class Contacto extends Model{
    public $nombre;
    public $email;
    public $telefono;
    public $direccion;
    public $asunto;
    public $fecha;
    public $politicas;
    
    public function attributeLabels(): array {
        return [
            "nombre" => "Nombre completo",
            "email" => "Correo electronico",
            "telefono" => "Telefono",
            "direccion" => "Dirreccion completa",
            "asunto" => "¿Cual es tu pregunta?",
            "fecha" => "Fecha de nacimiento",
            "politicas" => "Aceptar politicas de privacidad",
        ];
    }
    
    public function rules() {
        return [
            [["nombre","email","telefono"],"required"],
            ["email","email"],
            ["nombre","string","length"=>[1,100]],
            ["politicas",'boolean'],
            ["fecha","string"],
            //["fecha","date","format"=>'dd/mm/yyyy'],
            [["direccion","asunto"],"safe"],
            ["politicas","comprobarpoliticas"], // chequear si has aceptado las politicas
        ];
    }
    
    
    /**
     * chequear que hayas aceptado las politicas
     * 
     */
    public function comprobarpoliticas($attribute,$params) {
        if($this->$attribute==0){
            $this->addError($attribute,"Es necesario que aceptes las politicas de privacidad");
        }
    }
    
    
    public function contact()
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo(Yii::$app->params['contacto'])
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->email => $this->nombre])
                ->setSubject("contacto")
                ->setTextBody($this->asunto)
                ->send();

            return true;
        }
        return false;
    }

}

