<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Productos;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContacto()
{
    $model = new \app\models\Contacto(); // objeto de tipo contacto
    //$datos=Yii::$app->request->post(); // leo todos los datos enviados y los coloco en $datos
    
    if ($model->load(Yii::$app->request->post())) {
        if ($model->contact()) {
                 return $this->render("mostrar");
        }
    }

    return $this->render('contacto', [
        'model' => $model,
    ]);
}
    
    
    public function actionOfertas() {
        $dataProvider = new ActiveDataProvider([
            "query" => Productos::find()->where(['oferta'=>1]),
        ]);
                
        return $this->render('oferta',[
            'dataProvider' => $dataProvider,
        ]);        
    }
    
    public function actionProductos() {
        $dataProvider = new ActiveDataProvider([
            "query" => Productos::find(),
            'pagination' => [
                'pageSize' => 2,
            ]
        ]);
                
        return $this->render('producto',[
            'dataProvider' => $dataProvider,
        ]);        
    }
    
    public function actionCategorias() {
        $dataProvider = new ActiveDataProvider([
            "query" => Productos::find()->select('categorias')->distinct(),
        ]);
                
        return $this->render('categorias',[
            'dataProvider' => $dataProvider,
        ]);        
    }
    
    public function actionVer($categoria) {
        $productos = Productos::find()->where(['categorias'=>$categoria]);
        
        $dataProvider= new ActiveDataProvider([
            'query' => $productos,
        ]);
        
        return $this->render('ver',[
            'dataProvider'=> $dataProvider,
            'categoria'=> $categoria,
        ]);
    }
    
    public function actionDonde() {
        
        return $this->render('donde');
    }
    
    public function actionSomos() {
        
        return $this->render('somos');
    }
    
    
    public function actionNuestros() {
        
        $fotos= Productos::find()
                ->select("foto")
                ->where(["oferta" => 1])
                ->asArray()
                ->all();
        
        $fotos= ArrayHelper::getColumn($fotos, "foto");
        
        $salida=[];
        foreach ($fotos as $src){
            $salida[]= Html::img("@web/imgs/$src");
        }
                
        return $this->render('nuestro',[
            "fotos" => $salida,
        ]);
    }
    
    public function actionInfo() {
        
        return $this->render('info');      
    }
    
    public function actionVermas($id) {
        
        // metodo 1
        //$salida=Productos::find()->where(["id"=>$id]);
       
        //metodo 2
        $salida= Productos::findOne($id);
        
        return $this->render("vermas",[
            "model" => $salida
        ]);
    }
    
}
