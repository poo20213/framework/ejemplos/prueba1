<?php
use yii\widgets\ListView;
?>
<h1 class="border rounded bg-secondary p-3 text-white text-center mb-5">
    Listado de Productos de Categoria <?= $categoria ?>
</h1>
<?php
echo ListView::widget([
    "dataProvider" => $dataProvider,
    "itemView" => "_ver",
    "itemOptions" => [
        'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
    ],
    "options" => [
        'class' => 'row',
    ],
    'layout'=>"{items}"
]);




