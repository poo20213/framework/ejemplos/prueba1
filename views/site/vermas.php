<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'nombre',
        'descripcion',
        'precio',
        'categoria',
        'oferta',
        [
            'attribute' => 'foto',
            'label'=>'foto',
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->foto;
                return Html::img($url,[
                    'class'=>'img-fluid mx-auto d-block',
                    'style'=> 'width:200px']); 
            }
        ],
    ],
]);
