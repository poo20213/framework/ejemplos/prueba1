<?php

use yii\grid\GridView;
use yii\helpers\Html;
?>

<h1 class="border rounded bg-secondary p-3 text-white text-center mb-5">Listado de productos</h1>
<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}<br>{pager}<br>{summary}',
    "pager"=>[
        "options" => ["class"=>"row col-6"],
        "linkOptions"=>["class"=>"p-2 d-block w-100 h-100"],
        "disabledPageCssClass"=>"p-2 rounded d-block col-1 border",
        "activePageCssClass"=>"bg-light",
        "nextPageCssClass"=>"rounded d-block col-1 border",
        "prevPageCssClass"=>"rounded d-block col-1 border",
        "pageCssClass"=>"rounded d-block col-1 p-0 border",
    ],

    'columns' => [
        'id',
        'nombre',
        'precio',
        'categorias',
        [
            'attribute' => 'foto',
            'label'=>'foto',
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->foto;
                return Html::img($url,[
                    'class'=>'img-fluid mx-auto d-block',
                    'style'=> 'width:200px']); 
            }
        ],
        'descripcion',
        'oferta',
    ],
    'options'=>[
        'class' => 'text-center'
    ]            
]);
?>